package com.classpath.ordersapimgmt.service;

import com.classpath.ordersapimgmt.model.Order;
import com.classpath.ordersapimgmt.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchOrderById(long orderId) {
        //Optional<Order> optionalOrder = this.orderRepository.findById(orderId);
        /*if (optionalOrder.isPresent()){
            return optionalOrder.get();
        }*/
        return this.orderRepository.findById(orderId)
                                        .orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));
        //throw new IllegalArgumentException("Invalid Order Id");
    }

    @Override
    public Order updateOrderById(long orderId, Order order) {
        return null;
    }

    @Override
    public void deleteOrderById(long orderId) {
        this.orderRepository.deleteById(orderId);

    }
}