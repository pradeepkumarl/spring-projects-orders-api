package com.classpath.ordersapimgmt.service;

import com.classpath.ordersapimgmt.model.Order;

import java.util.Set;

public interface OrderService {

    Order save(Order order);

    Set<Order> fetchOrders();

    Order fetchOrderById(long orderId);

    Order updateOrderById(long orderId, Order order);

    void deleteOrderById(long orderId);
}