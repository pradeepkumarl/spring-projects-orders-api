package com.classpath.ordersapimgmt.controller;

import com.classpath.ordersapimgmt.model.Order;
import com.classpath.ordersapimgmt.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Set;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    @ResponseStatus(OK)
    public Set<Order> fetchOrders(){
        log.info("Inside fetchOrders");
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public Order fetchOrderByID( @PathVariable("id") long orderId){
        return this.orderService.fetchOrderById(orderId);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        return this.orderService.save(order);
    }
}