package com.classpath.ordersapimgmt.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import static javax.persistence.GenerationType.AUTO;

@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
@Table(name="orders")
@EqualsAndHashCode(of = "orderId")
public class Order {
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @Setter @Getter
    @Min(value = 15000, message = "Price should be greater than 15000")
    @Max(value = 50000, message = "Price should be less than 50000")
    private double price;

    @Setter @Getter
    private LocalDate localDate;
}