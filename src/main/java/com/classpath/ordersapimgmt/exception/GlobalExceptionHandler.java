package com.classpath.ordersapimgmt.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> handleException(RuntimeException runtimeException){
        Error error = Error.builder().errorCode(112).errorMessage(runtimeException.getMessage()).build();
        return ResponseEntity.status(BAD_REQUEST).body(error);
    }
}

@Setter
@Getter
@Builder
@AllArgsConstructor
class Error{
    private int errorCode;
    private String errorMessage;
}

