package com.classpath.ordersapimgmt.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(name="app.loadUser", havingValue = "true", matchIfMissing = true)
    //Java style configuring bean
    public User user(){
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name="user")
    //Java style configuring bean
    public User beanCondition(){
        return new User();
    }

    @Bean
    @Conditional(CustomConditional.class)
    public User customCondition(){
        return  new User();
    }
}


class User {

}