package com.classpath.ordersapimgmt.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;
import static org.springframework.boot.actuate.health.Status.UP;

@Configuration
public class ApplicationHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        //perform the sanity check for verifying if the service is up.
        //perform a select query if you are using a DB
        // Check with the message broker
        return Health.status(UP).withDetail("DB Service", "DB service is UP").build();
    }
}
@Configuration
class KafkaHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        //perform the sanity check for verifying if the service is up.
        //perform a select query if you are using a DB
        // Check with the message broker
        return Health.status(UP).withDetail("Kafka Service", "Kafka service is UP").build();
    }
}